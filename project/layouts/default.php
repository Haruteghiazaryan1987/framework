<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <link rel="stylesheet" href="/project/webroot/styles.css">
</head>
<body>
<?= $content ?>
</body>
<script src="/project/webroot/main.js"></script>
</html>
