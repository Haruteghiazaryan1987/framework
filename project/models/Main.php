<?php

namespace Project\Models;

use Core\Model;

class Main extends Model
{
    /**
     * @param array $params
     * @return array|int|null
     */
    public function insert(array $params): array|int|null
    {
        return $this->query("INSERT INTO user (name) VALUES (:name)", $params);
    }

    /**
     * @param array $params
     * @return array|int|null
     */
    public function getById(array $params): array|int|null
    {
        return $this->query("SELECT * FROM user WHERE id=:id", $params);
    }

    /**
     * @return array|int|null
     */
    public function getAll(): array|int|null
    {
        return $this->query("SELECT * FROM user");
    }

    /**
     * @param array $params
     * @return array|int|null
     */
    public function delete(array $params): array|int|null
    {
        return $this->query("DELETE FROM user WHERE `ID` = :id", $params);
    }
}
