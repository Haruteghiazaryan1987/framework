<?php

namespace Project\Controllers;

use \Core\Controller;
use Core\Page;
use Project\Models\Main;

class MainController extends Controller
{
    /**
     * @return Page
     */
    public function index(): Page
    {
        $this->title = 'Main!';

//        $main = new Main(); // Test model for base check
//        $user = $main->getAll();
        return $this->render('main/index');
    }
}
