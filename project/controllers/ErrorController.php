<?php

namespace Project\Controllers;

use \Core\Controller;
use Core\Page;

class ErrorController extends Controller
{
    /**
     * @return Page
     */
    public function notFound(): Page
    {
        $this->title = 'Not Found';

        return $this->render('error/notFound');
    }
}
