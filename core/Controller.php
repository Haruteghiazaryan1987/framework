<?php

namespace Core;

class Controller
{
    protected string $layout = 'default';

    /**
     * @param $view
     * @param array $data
     * @return Page
     */
    protected function render($view, array $data = []): Page
    {
        return new Page($this->layout, $this->title, $view, $data);
    }
}
