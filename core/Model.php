<?php

namespace Core;

use PDO;
use PDOException;

class Model
{
    private PDO $pdo;
    private object $statement;
    private array $parameters = [];
    private bool $isConnect;

    public function __construct()
    {
        $this->connect();
    }

    /**
     * @return void
     */
    private function connect(): void
    {
        try {
            $this->pdo = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME . ";charset=" . DB_CHARSET, DB_USER, DB_PASS);

            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->isConnect = true;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /**
     * @param string $query
     * @param array $parameters
     * @return void
     */
    private function init(string $query, array $parameters = []): void
    {
        if (!$this->isConnect) {
            $this->connect();
        }
        try {
            $this->statement = $this->pdo->prepare($query);
            $this->bind($parameters);

            if (!empty($this->parameters)) {
                foreach ($this->parameters as $param => $value) {
                    if (is_int($value[1])) {
                        $type = PDO::PARAM_INT;
                    } elseif (is_bool($value[1])) {
                        $type = PDO::PARAM_BOOL;
                    } elseif (is_null($value[1])) {
                        $type = PDO::PARAM_NULL;
                    } else {
                        $type = PDO::PARAM_STR;
                    }

                    $this->statement->bindValue($value[0], $value[1], $type);
                }
            }
            $this->statement->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        $this->parameters = [];
    }

    /**
     * @param array $parameters
     * @return void
     */
    private function bind(array $parameters): void
    {
        if (!empty($parameters)) {
            $columns = array_keys($parameters);

            foreach ($columns as $i => $column) {
                $this->parameters[sizeof($this->parameters)] = [':' . $column, $parameters[$column]];

            }
        }
    }

    /**
     * @return void
     */
    public function closeConnect(): void
    {
        $this->pdo = null;
    }

    /**
     * @param string $query
     * @param array $parameters
     * @param int $mode
     * @return array|int|null
     */
    public function query(string $query, array $parameters = [], int $mode = PDO::FETCH_ASSOC): int|array|null
    {
        $query = trim(str_replace('\r', ' ', $query));

        $this->init($query, $parameters);
        $rowStatement = explode(' ', preg_replace('/\s+|\t+|\n+/', ' ', $query));
        $statement = strtolower($rowStatement[0]);
        if ($statement === 'select' || $statement === 'show') {
            return $this->statement->fetchAll($mode);
        } elseif ($statement === 'insert' || $statement === 'update' || $statement === 'delete') {
            return $this->statement->rowCount();
        } else {
            return null;
        }
    }

    /**
     * @return string
     */
    public function lastInsertId(): string
    {
        return $this->statement->lastInsertId();
    }















    /*private function query($sql, $params = [])
    {
        $stmt = $this->pdo->prepare($sql);
//            dd($stmt);
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                $stmt->bindValue(':' . $key, $value);
            }
        }
        $stmt->execute();

        return $stmt;
    }*/

//    protected function findMany($sql, $params = [])
//    {
//        $result = $this->query($sql, $params);
////            dd($result);
//        return $result->fetchAll(PDO::FETCH_ASSOC);
//    }

//    protected function findOne($sql, $params = [])
//    {
//        $result = $this->query($sql, $params);
////            dd($result);
//        return $result->fetchColumn(1);
//    }

}
