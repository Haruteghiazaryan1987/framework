<?php

namespace Core;

use Exception;

error_reporting(E_ALL);
ini_set('display_errors', 'on');

require_once $_SERVER['DOCUMENT_ROOT'] . '/project/config/connection.php';

spl_autoload_register(function ($class) {
    preg_match('#(.+)\\\\(.+?)$#', $class, $match);
    $nameSpace = str_replace('\\', DIRECTORY_SEPARATOR, strtolower($match[1]));
    $className = $match[2];

    $path = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $nameSpace . DIRECTORY_SEPARATOR . $className . '.php';

    if (file_exists($path)) {
        require_once $path;

        if (class_exists($class, false)) {
            return true;
        } else {
            throw new Exception("Class $class not found in $path. Check the spelling of the class name inside the specified file.");
        }
    } else {
        throw new Exception("File $path not found for class $class. ");
    }
});

$routes = require $_SERVER['DOCUMENT_ROOT'] . '/project/config/routes.php';
$track = (new Router)->getTrack($routes, $_SERVER['REQUEST_URI']);
$page = (new Dispatcher)->getPage($track);
echo (new View)->render($page);
